strings_idioma = 
		{ 
			'es' : {
				'title' : 'Gestión Asignatura IU',
				'Autenticar':'Autenticar',
				'Portal':'Portal de Gestión',
				'usernotregister':'Usuario no Autenticado',
				'español' : 'castellano', 
				'ingles' : 'ingles',
				'gallego' : 'gallego',
				'login' : 'Nombre de usuario',
				'registrar' : 'Registrar',
				'password' : 'Contraseña',
				'hoy' : 'Hoy'
			},