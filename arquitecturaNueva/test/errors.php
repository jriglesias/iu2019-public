<?php
$errors = array (
'codigo' => 'descripcion',
// mensajes de exito generales (0000-00020)
'00000' => 'Inserción realizada con éxito',
// mensajes de error generales (00021-00050)
'00005' => 'Error de conexión a la BD',
'00006' => 'Error al ejecutar una operación sobre la BD',
'00050' => 'Método no permitido',
// mensajes de exito de clase model usuario (00050-00070)
'00051' => 'login y password correctos',
'00052' => 'La operación seleccionada se ha realizado correctamente',
// mensajes de error de clase model usuario (00070-00099)
'00071' => 'El usuario ya existe',
'00072' => 'El login no existe',
'00073' => 'La password para este usuario no es correcta'






);
?>