<?php 
$strings = 
array(
	'Portal de Gestión' => 'Management website',
	'Usuario no autenticado' => 'User not logged',
	'Ejemplo arquitectura IU' => 'IU architecture example',
	'Login' => 'Login', 
	'idioma' => 'Language',
	'Usuario' => 'User',
	'Ejemplo' => 'Example',
	'Otro' => 'Other',
	'INGLES' => 'ENGLISH',
	'ESPAÑOL' => 'SPANISH',
	'Volver' => 'Back',
	'Registro' => 'Register',
	'Gestión Asignatura IU' => 'User Interface Management',
	'Insertar usuario' => 'add user',
	'ADD' => 'ADD',
	'SHOWALL' => 'SHOLL ALL',
	'SHOWCURRENT' => 'VIEW DETAILS',
	'EDIT' => 'EDIT',
	'Gestión Usuarios' => 'User management',
	'SEARCH' => 'SEARCH',
	'DELETE' => 'DELETE',
	//-------------------- model messages
	'Error de gestor de base de datos' => 'xxxx',
	'Inserción fallida: el elemento ya existe' => 'yyyy',
	'Inserción realizada con éxito' => 'Success insert',
	'Borrado realizado con éxito' => 'Delete succesfully',
	'Actualización realizada con éxito' => 'update succesfully',
	'El login no existe' => 'The login dont exists',
	'La password para este usuario no es correcta' => 'Incorrect password for this login',
	'El usuario ya existe' => 'User exists'
 )
;
 ?>
