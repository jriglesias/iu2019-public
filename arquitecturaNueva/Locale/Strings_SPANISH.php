<?php 
$strings = 
array(
	'Portal de Gestión' => 'Portal de Gestión',
	'Usuario no autenticado' => 'Usuario no autenticado',
	'Ejemplo arquitectura IU' => 'Ejemplo arquitectura IU',
	'Login' => 'Login',
	'idioma' => 'Idioma',
	'Usuario' => 'Usuario',
	'Ejemplo' => 'Ejemplo',
	'Otro' => 'Otro',
	'INGLES' => 'INGLES',
	'ESPAÑOL' => 'ESPAÑOL',
	'Volver' => 'Volver',
	'Registro' => 'Registro',
	'Gestión Asignatura IU' => 'Gestión Asignatura IU',
	'Insertar usuario' => 'Insertar usuario',
	'ADD' => 'Añadir',
	'SHOWALL' => 'Mostrar todos',
	'EDIT' => 'EDITAR',
	'Gestión Usuarios' => 'Gestión Usuarios',
	'SEARCH' => 'BUSCAR',
	'DELETE' => 'BORRAR',
	'SHOWCURRENT' => 'DETALLE',
	//------------------- mensajes modelo
	'Error de gestor de base de datos' => 'Error de gestor de base de datos',
	'Inserción fallida: el elemento ya existe' =>'Inserción fallida: el elemento ya existe',
	'Inserción realizada con éxito' => 'Inserción realizada con éxito',
	'Borrado realizado con éxito' => 'Borrado realizado con éxito',
	'Actualización realizada con éxito' => 'Actualización realizada con éxito',
	'El login no existe' => 'El login no existe',
	'La password para este usuario no es correcta' => 'La password para este usuario no es correcta',
	'El usuario ya existe' => 'El usuario ya existe'
)
;
?>
