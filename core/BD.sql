-- SCRIPT DE LA BASE DE DATOS 
-- 
DROP DATABASE IF EXISTS `iu2019-libro`;
CREATE DATABASE `iu2019-libro` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
--
-- SELECCIONAMOS PARA USAR
--
USE `iu2019-libro`;
--
-- DAMOS PERMISO USO Y BORRAMOS EL USUARIO QUE QUEREMOS CREAR POR SI EXISTE
--
GRANT USAGE ON *.* TO `iu2019-libro`@`localhost`;
DROP USER `iu2019-libro`@`localhost`;
--
-- CREAMOS EL USUARIO Y LE DAMOS PASSWORD,DAMOS PERMISO DE USO Y DAMOS PERMISOS SOBRE LA BASE DE DATOS.
--
CREATE USER IF NOT EXISTS `iu2019-libro`@`localhost` IDENTIFIED BY 'pass2019';
GRANT USAGE ON *.* TO `iu2019-libro`@`localhost` REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `iu2019-libro`.* TO `iu2019-libro`@`localhost` WITH GRANT OPTION;
--
--
-- Estructura de tabla para la tabla `usuarios`
--
CREATE TABLE `usuarios` (
  `id` int(4) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `clave` varchar(160) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

